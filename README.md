## Detect content illustrations

The notebook contained in this repository lets you test our illustration detection model on your own PDFs. 
Clone or download. Run  `pip install -r requirements.txt` to install the required modules

If you do not have a PDF of an early modern print at hand you can use test.pdf included in the repo.

